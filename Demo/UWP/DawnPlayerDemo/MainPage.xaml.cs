﻿using System;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.Web.Http;
using Windows.Web.Http.Filters;
using FlvPlayer;

namespace FlvPlayerDemo
{
    public sealed partial class MainPage : Page
    {
        private uint playTaskID = 0;
        private FlvMedStrmSrc flvMedStrmSrc;
        private IInputStream vidStrm;

        public MainPage()
        {
            this.InitializeComponent();
            this.Loaded += MainPage_Loaded;
            this.Unloaded += MainPage_Unloaded;
        }

        private void MainPage_Unloaded(object sender, RoutedEventArgs e)
        {
            if (this.mediaElement != null)
            {
                this.mediaElement.Source = null;
            }
            if (this.flvMedStrmSrc != null)
            {
                this.flvMedStrmSrc.Dispose();
                this.flvMedStrmSrc = null;
            }
            if (this.vidStrm != null)
            {
                this.vidStrm.Dispose();
                this.vidStrm = null;
            }
        }

        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            this.Splitter.IsPaneOpen = true;
        }

        private async void OpenLocalFile(object sender, RoutedEventArgs e)
        {
            this.Splitter.IsPaneOpen = false;
            FileOpenPicker fileOpenPicker = new FileOpenPicker();
            fileOpenPicker.ViewMode = PickerViewMode.List;
            fileOpenPicker.SuggestedStartLocation = PickerLocationId.VideosLibrary;
            fileOpenPicker.FileTypeFilter.Add(".flv");
            var file = await fileOpenPicker.PickSingleFileAsync();
            if (file == null)
            {
                return;
            }
            this.mediaElement.Source = null;
            if (this.flvMedStrmSrc != null)
            {
                this.flvMedStrmSrc.Dispose();
                this.flvMedStrmSrc = null;
            }
            if (this.vidStrm != null)
            {
                this.vidStrm.Dispose();
                this.vidStrm = null;
            }
            var taskID = ++this.playTaskID;
            IRandomAccessStream fileStream = null;
            try
            {
                fileStream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read);
            }
            catch (Exception)
            {
            }
            if (taskID != this.playTaskID)
            {
                if (fileStream != null)
                {
                    fileStream.Dispose();
                    fileStream = null;
                }
                return;
            }
            if (fileStream == null)
            {
                this.ShowMessage("Failed to open media");
                return;
            }
            FlvMedStrmSrc fmss = null;
            try
            {
                fmss = await FlvMedStrmSrc.CrtFromRandAccStrmAsy(fileStream);
            }
            catch (Exception)
            {
                fileStream.Dispose();
                fileStream = null;
            }
            if (taskID != this.playTaskID)
            {
                if (fmss != null)
                {
                    fmss.Dispose();
                    fmss = null;
                }
                if (fileStream != null)
                {
                    fileStream.Dispose();
                    fileStream = null;
                }
                return;
            }
            if (fmss == null)
            {
                this.ShowMessage("Failed to open media");
                return;
            }
            this.vidStrm = fileStream;
            this.flvMedStrmSrc = fmss;
            mediaElement.SetMediaStreamSource(flvMedStrmSrc.Source);
        }

        private async void OpenUrl(object sender, RoutedEventArgs e)
        {
            var url = this.UrlInputBox.Text.Trim();
            if (url.Length == 0)
            {
                return;
            }
            Uri uri = null;
            try
            {
                uri = new Uri(url);
            }
            catch (Exception)
            {
                this.ShowMessage("Bad url");
                return;
            }
            this.mediaElement.Source = null;
            if (this.flvMedStrmSrc != null)
            {
                this.flvMedStrmSrc.Dispose();
                this.flvMedStrmSrc = null;
            }
            if (this.vidStrm != null)
            {
                this.vidStrm.Dispose();
                this.vidStrm = null;
            }
            var taskID = ++this.playTaskID;
            this.Splitter.IsPaneOpen = false;
            IInputStream fileStream = null;
            try
            {
                var hpf = new HttpBaseProtocolFilter();
                hpf.CacheControl.ReadBehavior = HttpCacheReadBehavior.MostRecent;
                hpf.CacheControl.WriteBehavior = HttpCacheWriteBehavior.NoCache;
                var httpClient = new HttpClient(hpf);
                fileStream = await httpClient.GetInputStreamAsync(uri);
            }
            catch (Exception)
            {
            }
            if (taskID != this.playTaskID)
            {
                if (fileStream != null)
                {
                    fileStream.Dispose();
                    fileStream = null;
                }
                return;
            }
            if (fileStream == null)
            {
                this.ShowMessage("Failed to open media");
                return;
            }
            FlvMedStrmSrc fmss = null;
            try
            {
                fmss = await FlvMedStrmSrc.CrtFromInpStrmAsy(fileStream);
            }
            catch (Exception)
            {
                fileStream.Dispose();
                fileStream = null;
            }
            if (taskID != this.playTaskID)
            {
                if (fmss != null)
                {
                    fmss.Dispose();
                    fmss = null;
                }
                if (fileStream != null)
                {
                    fileStream.Dispose();
                    fileStream = null;
                }
                return;
            }
            if (fmss == null)
            {
                this.ShowMessage("Failed to open media");
                return;
            }
            this.vidStrm = fileStream;
            this.flvMedStrmSrc = fmss;
            mediaElement.SetMediaStreamSource(flvMedStrmSrc.Source);
        }

        private void OnMediaEnd(object sender, RoutedEventArgs e)
        {
            this.mediaElement.Source = null;
            if (this.flvMedStrmSrc != null)
            {
                this.flvMedStrmSrc.Dispose();
                this.flvMedStrmSrc = null;
            }
            if (this.vidStrm != null)
            {
                this.vidStrm.Dispose();
                this.vidStrm = null;
            }
            this.ShowMessage("OnMediaEnd");
        }

        private void OnMediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            this.mediaElement.Source = null;
            if (this.flvMedStrmSrc != null)
            {
                this.flvMedStrmSrc.Dispose();
                this.flvMedStrmSrc = null;
            }
            if (this.vidStrm != null)
            {
                this.vidStrm.Dispose();
                this.vidStrm = null;
            }
            this.ShowMessage("OnMediaFailed");
        }

        private async void ShowMessage(string message)
        {
            var dialog = new MessageDialog(message);
            await dialog.ShowAsync();
        }
    }
}
