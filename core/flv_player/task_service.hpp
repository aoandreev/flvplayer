#ifndef FLV_PLAYER_TASK_SERVICE_HPP
#define FLV_PLAYER_TASK_SERVICE_HPP

#include <experimental/resumable>
#include <functional>
#include <thread>

namespace flv_plr {

	struct task_service {
		virtual void post_task(std::function<void()>&& task) = 0;
		virtual std::thread::id get_thread_id() = 0;
		virtual ~task_service() {}
	};

	namespace impl {
		class swch_task_serv_awtr {
			task_service* tsk_service;
		public:
			swch_task_serv_awtr(task_service* service);
			bool await_ready();
			void await_resume();
			void await_suspend(std::experimental::coroutine_handle<> coro);
		};
	}

	impl::swch_task_serv_awtr switch_to_task_service(task_service* tsk_service);

} // namespace flv_plr

#endif
