
#ifndef FLV_PLAYER_DEFAULT_TASK_SERVICE_HPP
#define FLV_PLAYER_DEFAULT_TASK_SERVICE_HPP

#include <condition_variable>
#include <memory>
#include <mutex>
#include <queue>

#include "task_service.hpp"

namespace flv_plr {
	namespace impl
	{

		struct deflt_task_serv_cntxt {
			std::queue<std::function<void()>> task_queue;
			std::mutex task_queue_mtx;
			std::condition_variable task_queue_cv;
		};

	} // namespace impl

	class default_task_service : public task_service {
	public:
		default_task_service();
		virtual ~default_task_service();
		virtual void post_task(std::function<void()>&& task);
		virtual std::thread::id get_thread_id();
	private:
		std::shared_ptr<impl::deflt_task_serv_cntxt> service_ctx;
		std::thread::id thread_id;
	};

} // namespace flv_plr

#endif
