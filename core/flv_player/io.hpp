#ifndef FLV_PLAYER_IO_HPP
#define FLV_PLAYER_IO_HPP

#include <cstdint>
#include <future>

using namespace Windows::Storage::Streams;

namespace flv_plr {
	namespace io {

		struct read_stream_proxy {
			virtual ~read_stream_proxy() {}
			virtual bool can_seek() const = 0;
			virtual std::future<std::uint32_t> read(std::uint8_t* buf, std::uint32_t size) = 0;
			virtual void seek(std::uint64_t pos) = 0;
		};

		class rand_acc_rd_strm_prx : public read_stream_proxy {
		public:
			rand_acc_rd_strm_prx(IRandomAccessStream^ stream);
			virtual ~rand_acc_rd_strm_prx();
			virtual bool can_seek() const;
			virtual std::future<std::uint32_t> read(std::uint8_t* buf, std::uint32_t size);
			virtual void seek(std::uint64_t pos);
		private:
			IRandomAccessStream^ target;
		};

		class input_read_stream_proxy : public read_stream_proxy {
		public:
			input_read_stream_proxy(IInputStream^ stream);
			virtual ~input_read_stream_proxy();
			virtual bool can_seek() const;
			virtual std::future<std::uint32_t> read(std::uint8_t* buf, std::uint32_t size);
			virtual void seek(std::uint64_t pos);
		private:
			IInputStream^ target;
		};

	} // namespace io
} // namespace flv_plr

#endif
