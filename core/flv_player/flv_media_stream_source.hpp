﻿
#ifndef FLV_PLAYER_FLV_MEDIA_STREAM_SOURCE_HPP
#define FLV_PLAYER_FLV_MEDIA_STREAM_SOURCE_HPP

#include <memory>

#include "flv_player.hpp"
#include "io.hpp"
#include "task_service.hpp"

using namespace Windows::Foundation;
using namespace Windows::Media::Core;
using namespace Windows::Storage::Streams;

using namespace flv_plr;
using namespace flv_plr::io;

namespace FlvPlayer {

	public ref class FlvMedStrmSrc sealed {
	private:
		std::shared_ptr<flv_player> player;
		MediaStreamSource^ mss;
	private:
		FlvMedStrmSrc();
		void init(const std::shared_ptr<flv_player>& player, MediaStreamSource^ mss);
	public:
		static IAsyncOperation<FlvMedStrmSrc^>^ CrtFromInpStrmAsy(IInputStream^ inputStream);
		static IAsyncOperation<FlvMedStrmSrc^>^ CrtFromRandAccStrmAsy(IRandomAccessStream^ randomAccessStream);
		property MediaStreamSource^ Source
		{
			MediaStreamSource^ get();
		}
		virtual ~FlvMedStrmSrc();
	private:
		static IAsyncOperation<FlvMedStrmSrc^>^ crt_from_rd_stream_prx_asy(const std::shared_ptr<read_stream_proxy>& stream_proxy);
		void on_starting(MediaStreamSource^ sender, MediaStreamSourceStartingEventArgs^ args);
		void on_sample_requested(MediaStreamSource^ sender, MediaStreamSourceSampleRequestedEventArgs^ args);
		std::future<void> handle_starting(MediaStreamSource^ sender, MediaStreamSourceStartingEventArgs^ args);
		std::future<void> handle_sample_requested(MediaStreamSource^ sender, MediaStreamSourceSampleRequestedEventArgs^ args);
		Windows::Foundation::EventRegistrationToken starting_event_token;
		Windows::Foundation::EventRegistrationToken smpl_reqed_event_tok;
	};

} // namespace FlvPlayer

#endif
