#include <memory>

#include "task_service.hpp"

namespace flv_plr {

	namespace impl {

		swch_task_serv_awtr::swch_task_serv_awtr(task_service* service)
			: tsk_service(service)
		{}

		bool swch_task_serv_awtr::await_ready()
		{
			return std::this_thread::get_id() == this->tsk_service->get_thread_id();
		}

		void swch_task_serv_awtr::await_resume()
		{
		}

		void swch_task_serv_awtr::await_suspend(std::experimental::coroutine_handle<> coro)
		{
			this->tsk_service->post_task([coro]() {
				coro.resume();
			});
		}

	} // namespace impl

	impl::swch_task_serv_awtr switch_to_task_service(task_service* tsk_service)
	{
		return impl::swch_task_serv_awtr{ tsk_service };
	}

} // namespace flv_plr
