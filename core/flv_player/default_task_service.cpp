
#include <thread>

#include "default_task_service.hpp"

namespace flv_plr {
	namespace impl
	{

		void task_thread_proc(const std::shared_ptr<deflt_task_serv_cntxt>& service_ctx)
		{
			while (true) {
				std::function<void()> task;
				{
					std::unique_lock<std::mutex> lck(service_ctx->task_queue_mtx);
					service_ctx->task_queue_cv.wait(lck, [&service_ctx]() {
						return !service_ctx->task_queue.empty();
					});
					task = std::move(service_ctx->task_queue.front());
					service_ctx->task_queue.pop();
				}
				if (task == nullptr) {
					break;
				}
				task();
			}
		}

	} // namespace impl

	default_task_service::default_task_service()
		: service_ctx(std::make_shared<impl::deflt_task_serv_cntxt>())
	{
		auto ctx = this->service_ctx;
		auto td = std::thread([ctx]() {
			impl::task_thread_proc(ctx);
		});
		this->thread_id = td.get_id();
		td.detach();
	}

	default_task_service::~default_task_service()
	{
		this->post_task(nullptr);
	}

	void default_task_service::post_task(std::function<void()>&& task)
	{
		{
			std::unique_lock<std::mutex> lck(this->service_ctx->task_queue_mtx);
			this->service_ctx->task_queue.push(std::move(task));
		}
		this->service_ctx->task_queue_cv.notify_one();
	}

	std::thread::id default_task_service::get_thread_id()
	{
		return this->thread_id;
	}

} // namespace flv_plr
